# Chapter 5: Key-Value Data

## Usage

Testing in Elixir console:
```shell
iex -S mix
```

### Map
Testing: `next_digit` function:
```shell
iex> Roman.next_digit({10, []})
{0, ["X"]}
iex> Roman.next_digit({8, []}) 
{0, ["I", "I", "I", "V"]}
iex> Roman.next_digit({158, []})
{0, ["I", "I", "I", "V", "L", "C"]}
```

Testing `convert` function:
```shell
iex> Roman.convert 3
:III
iex> Roman.convert 64
:LXIV
```

Testing `map` function (this function builds a map of roman numbers to digits):
```shell
iex(10)> map = Roman.map
%{
  XV: 15,
  DCCCV: 805,
  DXXXIX: 539,
  DCCXIV: 714,
  LXXII: 72,
  VI: 6,
  ...
}
```

### Struct

Test sample struct:

```shell
iex> exports Shape
__struct__/0     __struct__/1

iex> Shape.__struct__
%Shape{points: nil, stroke: nil, fill: nil}

iex> shape = Shape.__struct__(fill: :white, stroke: :black, points: [Point.new(1, 2), Point.new(2, 2), Point.new(2, 1)])
%Shape{points: [{1, 2}, {2, 2}, {2, 1}], stroke: :black, fill: :white}
```

Structs can be used as maps:
```shell
iex> shape.fill
:white
iex> %{points: [first, second, third]} = shape
%Shape{points: [{1, 2}, {2, 2}, {2, 1}], stroke: :black, fill: :white}
iex> first
{1, 2}
```

*Struct* syntax can be used to create a new struct:
```shell
iex> %Shape{fill: :white, points: [{1, 2}, {2, 2}, {2, 1}], stroke: :black}
%Shape{points: [{1, 2}, {2, 2}, {2, 1}], stroke: :black, fill: :white}
```

Create struct using default values:
```shell
iex> Shape.__struct__
%Shape{points: nil, stroke: nil, fill: nil}
```