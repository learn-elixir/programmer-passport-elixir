defmodule Shape do
  @enforce_keys [:points]
  defstruct([
    :points,
    stroke: :blue,
    fill: :black
  ])
end
