# Chapter 2 

## Atoms, Pattern Matching, and Erlang Access

To start `iex` cli execute following command:
```shell
iex -S mix
```
### Atoms

Run following commands in iex:
```shell
iex> Color.hex_code(:white)
"#ffffff"
```
```shell
iex> Color.hex_code(:black)
"#000000"
```

When try to call this function with unexpected color, error will be thrown:
```shell
iex> Color.hex_code(:blue) 
** (FunctionClauseError) no function clause matching in Color.hex_code/1    
    
    The following arguments were given to Color.hex_code/1:
    
        # 1
        :blue
    
    Attempted function clauses (showing 2 out of 2):
    
        def hex_code(:white)
        def hex_code(:black)
    
    (graphics 0.1.0) lib/color.ex:2: Color.hex_code/1
    iex:4: (file)
```

Can add custom error by adding following function
```elixir
def hex_code(_other) do
  raise "unsupported color"
end
```

After that calling function with unexpected color will produce better error message:
```shell
iex(2)> recompile
Compiling 1 file (.ex)
:ok
iex(3)> Color.hex_code(:yellow)
** (RuntimeError) unsupported color
    (graphics 0.1.0) lib/color.ex:9: Color.hex_code/1
    iex:3: (file)
```
