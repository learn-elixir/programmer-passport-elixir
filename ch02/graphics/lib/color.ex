defmodule Color do
  def hex_code(:white) do
    "#ffffff"
  end
  def hex_code(:black) do
    "#000000"
  end
  def hex_code(_other) do
    raise "unsupported color"
  end

  def hex_color(color) do
    cond do
      color == :white ->
        "#ffffff"
      color == :black ->
        "#000000"
      true ->
        :error
    end
  end

  def hex_code2(color) do
    case color do
      :white -> "#ffffff"
      :black -> "#000000"
      _ -> :error
    end
  end
end
