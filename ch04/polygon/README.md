# Chapter 4: Lists and Algorithms

Polygon module to experiment with lists, higher order functions, recursion etc.

## Usage

To test the code start iex console:
```shell
iex -S mix
```

And execute following commands:
```shell
iex(1)> r = Polygon.rectangle(Point.origin, 4, 2)
[{0, 0}, {2, 0}, {2, 4}, {0, 4}]
iex(2)> r |> Polygon.rotate(270, 2, 4)
[{4, 0}, {4, 2}, {0, 2}, {0, 0}]
```