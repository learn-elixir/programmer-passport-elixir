# Hello

Sample "Hello" application

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `hello` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:hello, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/hello>.

## Custom task

Project contains custom task `remind`.

To run the task execute following command:
```shell
mix remind
```

It will produce following output:
```shell
iex -S mix
-> Start a console with our project

mix test
-> Run tests

mix deps.get
-> Fetch dependencies.
```

Task includes short documentation which will be show in help command:
```shell
mix help
```

Which will produce following result:
```shell
mix help
mix                   # Runs the default task (current: "mix run")
...
mix remind            # Return a reminder for how to use our project.
...
```
